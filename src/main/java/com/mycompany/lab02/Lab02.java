/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab02;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class Lab02 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'X';
    static int row, col;

    static void printWelcome() {
        System.out.println("Welcome to Tic Tac Toe game");
    }

    static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();

        }
    }

    static void printTurn() {
        System.out.println(currentPlayer + " Turn");
    }

    static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row col :");
            row = kb.nextInt();
            col = kb.nextInt();
            if (table[row][col] == '-') {
                table[row][col] = currentPlayer;
                break;
            }
        }
    }

    static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    static boolean isWin() {
        if (checkRow()) {
            return true;
        } else if (checkCol()) {
            return true;
        } else if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;
    }

    static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    static boolean checkX1() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    static boolean checkX2() {
        for (int i = 0; i < 3; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    static boolean isDraw() {
        if (checkDraw()) {
            return true;
        }
        return false;
    }

    static boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    static void printWin() {
        System.out.println(currentPlayer + " Winner!!");
    }

    static void printDraw() {
        System.out.println("Draw!");
    }

    static void newTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = '-';
            }
        }
    }

    static boolean Continue() {
        System.out.print("Continue(y/n):");
        Scanner kb = new Scanner(System.in);
        String yn = kb.next();
        if (yn.equals("y")) {
            return true;
        } else if (yn.equals("n")) {
            return false;
        }
        return false;
    }

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            while (true) {
                printTable();
                printTurn();
                inputRowCol();
                if (isWin()) {
                    printTable();
                    printWin();
                    break;
                }
                if (isDraw()) {
                    printTable();
                    printDraw();
                    break;
                }

                switchPlayer();
            }
            newTable();
            if (Continue() == false) {
                break;
            }
        }
        System.out.println("End game");

    }

}
